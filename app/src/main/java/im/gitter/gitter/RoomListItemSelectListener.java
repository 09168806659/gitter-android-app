package im.gitter.gitter;

import im.gitter.gitter.models.RoomListItem;

public interface RoomListItemSelectListener {
    void onSelect(RoomListItem roomListItem);
}
