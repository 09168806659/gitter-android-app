package im.gitter.gitter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class LoginData {

    private final SharedPreferences prefs;

    public LoginData(Context activity) {
        this.prefs = activity.getSharedPreferences("gitter_login_prefs", Activity.MODE_PRIVATE);
    }

    public void setLoginData(String userId, String accessToken) {
        SharedPreferences.Editor settingsEditor = prefs.edit();
        settingsEditor.putString("user_id", userId);
        settingsEditor.putString("access_token", accessToken);
        settingsEditor.commit();
    }

    public boolean isLoggedIn() {
        return prefs.contains("user_id") && prefs.contains("access_token");
    }

    public String getUserId() {
        return prefs.getString("user_id", null);
    }

    public String getAccessToken() {
        return prefs.getString("access_token", null);
    }

    public void clear() {
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.commit();
    }
}
