package im.gitter.gitter.network;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;

import com.android.volley.Response;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import im.gitter.gitter.content.ModelFactory;
import im.gitter.gitter.models.Group;

public abstract class GroupsRequest extends ApiRequest<Set<String>> {

    private final ModelFactory modelFactory = new ModelFactory();
    private final ContentResolver contentResolver;

    public GroupsRequest(Context context, String path, Response.Listener<Set<String>> listener, Response.ErrorListener errorListener) {
        super(context, path, listener, errorListener);
        contentResolver = context.getContentResolver();
    }

    @Override
    protected Set<String> parseJsonInBackground(String jsonString) throws JSONException {
        JSONArray jsonArray = new JSONArray(jsonString);

        Map<String, ContentValues> groupsById = new HashMap<>();

        for (int i = 0; i < jsonArray.length(); i++) {
            Group group = modelFactory.createGroup(jsonArray.getJSONObject(i));
            groupsById.put(group.getId(), group.toContentValues());
        }

        writeToDatabaseInBackground(contentResolver, toArray(groupsById));

        return groupsById.keySet();
    }

    protected abstract void writeToDatabaseInBackground(ContentResolver contentResolver, ContentValues[] groups);

    private ContentValues[] toArray(Map<String, ContentValues> contentValuesById) {
        return contentValuesById.values().toArray(new ContentValues[contentValuesById.size()]);
    }
}
